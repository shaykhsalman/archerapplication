/**
 * Indicates that this module contains classes that need to be generated / processed.
 */
@ModuleGen(name = "vertxx-microservices", groupPackage = "com.fairbit.monera.storeservices")

package com.fairbit.monera.storeservices;

import io.vertx.codegen.annotations.ModuleGen;