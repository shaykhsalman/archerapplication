package com.fairbit.monera.storeservices;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.MongoClient;

public class StoreCRUDServiceImp implements StoreCRUDService {
    private static final String store_obj = "store";
    private static final Logger logger = LoggerFactory.getLogger(StoreCRUDServiceImp.class);

    private final MongoClient client;

    public StoreCRUDServiceImp(Vertx vertx, JsonObject config){
        JsonObject jsonObject = new JsonObject();

        //jsonObject.put("connection-string", "mongodb://user:user@ds237735.mlab.com:37735/vertx-ecommerce");
        jsonObject.put("connection_string", "mongodb://mongo:27017");
        jsonObject.put("db_name", "test");
        this.client = MongoClient.createShared(vertx, jsonObject);
    }
    @Override
    public void saveStore(Store store, Handler<AsyncResult<Void>> resultHandler) {
        client.save(store_obj, new JsonObject().put("_id", store.getSellerId())
            .put("name", store.getName())
                .put("description", store.getDescription())
                .put("openTime", store.getOpenTime()),
                ar-> {
                    if(ar.succeeded()){
                        logger.info("Succeeded in saving the store...");
                        resultHandler.handle(Future.succeededFuture());
                    }else{
                        resultHandler.handle(Future.failedFuture(ar.cause()));
                    }
                });
    }

    @Override
    public void retrieveStore(String sellerId, Handler<AsyncResult<Store>> resultHandler) {
        JsonObject query = new JsonObject().put("_id", sellerId);
        client.findOne(store_obj, query, null, ar -> {
            if (ar.succeeded()) {
                if (ar.result() == null) {
                    resultHandler.handle(Future.succeededFuture());

                } else {
                    Store store = new Store(ar.result().put("sellerId", ar.result().getString("_id")));
                    resultHandler.handle(Future.succeededFuture(store));
                }
            } else {
                resultHandler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    @Override
    public void removeStore(String sellerId, Handler<AsyncResult<Void>> resultHandler) {
        JsonObject query = new JsonObject().put("_id", sellerId);
        client.removeDocument(store_obj, query, ar -> {
            if (ar.succeeded()) {
                resultHandler.handle(Future.succeededFuture());
            } else {
                resultHandler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

}
