package com.fairbit.monera.storeservices;

import com.fairbit.monera.commonservices.BaseVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ProxyHelper;

import static com.fairbit.monera.storeservices.StoreCRUDService.SERVICE_ADDRESS;
import static com.fairbit.monera.storeservices.StoreCRUDService.SERVICE_NAME;

/**
 * To initiate and deploy RestVerticle
 *
 */
public class StoreVerticle extends BaseVerticle{
    private StoreCRUDService storeCRUDService;


    @Override
    public void start(Future<Void> startFuture) throws Exception {
        super.start();
        JsonObject config = new JsonObject();
//        System.out.println(config.encodePrettily());

        storeCRUDService = new StoreCRUDServiceImp(vertx, config);
        ProxyHelper.registerService(StoreCRUDService.class, vertx, storeCRUDService, SERVICE_ADDRESS);
        // publish service and deploy REST verticle
        publishEventBusService(SERVICE_NAME, SERVICE_ADDRESS, StoreCRUDService.class)
                .compose(servicePublished -> deployRestVerticle(storeCRUDService))
                .setHandler(startFuture.completer());
    }
    private Future<Void> deployRestVerticle(StoreCRUDService service) {
        Future<String> future = Future.future();
        vertx.deployVerticle(new RestStoreAPIVerticle(service),
                new DeploymentOptions().setConfig(config()),
                future.completer());
        return future.map(r -> null);

    }
}
