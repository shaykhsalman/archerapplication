package com.fairbit.monera.commonservices;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Example helper class. Executes SQL statement.
 */
public class ExampleHelper {
  public static void initData(Vertx vertx, JsonObject config) {
    vertx.executeBlocking(future -> {
      try {
        Connection connection = DriverManager.getConnection(config.getString("url"),
          config.getString("user"), config.getString("password"));
        connection.createStatement().executeUpdate("INSERT INTO product (`productId`, `sellerId`, `name`, `type`) VALUES ('A1763817', '112512', 'Fresh apple', '1.99', 'gresh fruits!', 'Food') ON DUPLICATE KEY UPDATE productId = productId");
        connection.createStatement().executeUpdate("INSERT INTO product (`productId`, `sellerId`, `name`, `type`) VALUES ('A1763818', '112513', 'Fresh apple', '1.99', 'Fresh fruits!', 'Food') ON DUPLICATE KEY UPDATE productId = productId");
        connection.createStatement().executeUpdate("INSERT INTO product (`productId`, `sellerId`, `name`, `type`) VALUES ('A1763819', '112514', 'Fresh apple', '1.99', 'Fresh fruits!', 'Food') ON DUPLICATE KEY UPDATE productId = productId");
        future.complete();
      } catch (SQLException ex) {
        ex.printStackTrace();
        future.fail(ex);
      }
    }, ar -> {
      if (ar.failed()) {
        ar.cause().printStackTrace();
      }
    });
  }
}
